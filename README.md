# Bookshop database
* [Subject](#subject)
* [Covered topics](#covered-topics)
* [Libraries](#libraries)

# Subject   :pushpin:
The goal is to create an app that stores books informations:
- Title
- Author
- Year
- ISBN

The user can :
- View all records
- Search an entry
- Add an entry
- Update an entry
- Delete an entry
- Close

# Covered topics
- Database with Postgres
- Widgets

# Libraries :books:
- sqlite3
- Tkinter
- pyinstaller (pyinstaller --onefile -windowed main.py)
