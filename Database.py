import sqlite3

class Database :
    def __init__(self, database):
        ## 1. Connect to a database
        self.connect = sqlite3.connect(database)
        #   2. Create a cursor object
        self.cursor = self.connect.cursor()
        #   3. Write an SQL query
        ## Create a table
        self.cursor.execute("CREATE TABLE IF NOT EXISTS books (id INTEGER PRIMARY KEY ,title TEXT, author TEXT, year INTEGER, ISBN INTEGER )")
        # 4. Save changes
        self.connect.commit()

    def __del(self):
        #   5. Close database connection
        self.connect.close()

    # Insert in database
    def insert(self, title, author, year, ISBN):
        self.cursor.execute("INSERT INTO books VALUES (NULL, ?, ?, ?, ?)", (title, author, year, ISBN))  # single quote !
        self.connect.commit()

    # Read from data base
    def view_all(self):
        self.cursor.execute("SELECT * FROM books")
        rows = self.cursor.fetchall()
        self.connect.commit()
        return rows

    # Delete from database
    def delete(self, id):
        self.cursor.execute("DELETE FROM books WHERE id=?", (id,))
        self.connect.commit()


    def search(self, title="", author="", year="", ISBN=""):
        self.cursor.execute("SELECT * FROM books WHERE title=? OR author=? OR year=? OR ISBN=?",
                       (title, author, year, ISBN))
        rows = self.cursor.fetchall()
        self.connect.commit()
        return rows

    # # Update database
    def update(self, id, title, author, year, ISBN):
        self.cursor.execute("UPDATE books SET title=?, author=?, year=?, ISBN=? WHERE id=?",
                       (title, author, year, ISBN, id))
        self.connect.commit()
        self.connect.close()