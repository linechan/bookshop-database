import tkinter
from Database import Database

class Window:
    def __init__(self, title):
        self.database = Database("books.db")
        self.window = tkinter.Tk()
        self.window.wm_title(title)
        # Labels
        label_title = tkinter.Label(self.window,text="Title")
        label_title.grid(row=0, column=0)
        label_year = tkinter.Label(self.window,text="Year")
        label_year.grid(row=1, column=0)
        label_author = tkinter.Label(self.window, text="Author")
        label_author.grid(row=0, column=2)
        label_isbn = tkinter.Label(self.window, text="ISBN")
        label_isbn.grid(row=1, column=2)

        # Inputs
        self.string_title = tkinter.StringVar()
        self.entry_title = tkinter.Entry(self.window, textvariable=self.string_title)
        self.entry_title.grid(row=0, column=1)

        self.string_year = tkinter.StringVar()
        self.entry_year = tkinter.Entry(self.window, textvariable=self.string_year)
        self.entry_year.grid(row=1, column=1)

        self.string_author = tkinter.StringVar()
        self.entry_author = tkinter.Entry(self.window, textvariable=self.string_author)
        self.entry_author.grid(row=0, column=3)

        self.string_ISBN = tkinter.StringVar()
        self.entry_ISBN = tkinter.Entry(self.window, textvariable=self.string_ISBN)
        self.entry_ISBN.grid(row=1, column=3)

        # Create a list box to display results
        self.list_box = tkinter.Listbox(self.window, height=6, width=35)
        self.list_box.grid(row=2, column=0, rowspan=6, columnspan=2)

        scrollbar = tkinter.Scrollbar(self.window)
        scrollbar.grid(row=2, column=2, rowspan=6)

        scrollbar.configure(command=self.list_box.yview)
        self.list_box.bind('<<ListboxSelect>>', self.get_selected_row)

        # Create buttons
        button_view_all = tkinter.Button(self.window, text="View all", width=12, command=self.cmd_view_all)
        button_view_all.grid(row=3, column=3)

        button_search = tkinter.Button(self.window, text="Search book", width=12, command=self.cmd_search)
        button_search.grid(row=4, column=3)

        button_add = tkinter.Button(self.window, text="Add book", width=12, command=self.cmd_insert)
        button_add.grid(row=5, column=3)

        button_update = tkinter.Button(self.window, text="Update book", width=12, command=self.cmd_update)
        button_update.grid(row=6, column=3)

        button_delete = tkinter.Button(self.window, text="Delete", width=12, command=self.cmd_delete)
        button_delete.grid(row=7, column=3)

        button_close = tkinter.Button(self.window, text="Close", width=12, command=self.window.destroy)
        button_close.grid(row=8, column=3)

        self.selected_tuple = ("", "", "", "", "")

    def run(self):
        self.window.mainloop()

    # Callback functions
    def cmd_view_all(self):
        # Clean previous entry
        self.list_box.delete(0, 'end')

        for row in self.database.view_all():
            self.list_box.insert('end', row)

    def cmd_search(self):
        # Clean previous entry
        self.list_box.delete(0, 'end')
        for row in self.database.search(title = self.string_title.get(),
                                        author = self.string_author.get(),
                                        year = self.string_year.get(),
                                        ISBN = self.string_ISBN.get()):
            self.list_box.insert('end', row)

    def cmd_insert(self):
        print("INSERT")
        self.database.insert(title = self.string_title.get(),
                             author = self.string_author.get(),
                             year = self.string_year.get(),
                             ISBN = self.string_ISBN.get())
        self.list_box.delete(0, 'end')
        # Show entry in the listbox : insert as a tuple
        self.list_box.insert('end', (self.string_title.get(),
                                     self.string_author.get(),
                                     self.string_year.get(),
                                     self.string_ISBN.get()))

    def cmd_update(self):
        self.database.update(self.selected_tuple[0],
                             self.string_title.get(),
                             self.string_author.get(),
                             self.string_year.get(),
                             self.string_ISBN.get())

    def cmd_delete(self):
        # Need to grab the id for the backend.delete() function
        self.database.delete(self.selected_tuple[0])

    # Event function
    def get_selected_row(self, event):
        try:
            # Returns index of selected row
            index = self.list_box.curselection()[0]  # index position in the result
            # Convert to tuple
            self.selected_tuple = self.list_box.get(index)

            # Fill entries
            self.entry_title.delete(0, 'end')
            self.entry_title.insert('end', self.selected_tuple[1])

            self.entry_author.delete(0, 'end')
            self.entry_author.insert('end', self.selected_tuple[2])

            self.entry_year.delete(0, 'end')
            self.entry_year.insert('end', self.selected_tuple[3])

            self.entry_ISBN.delete(0, 'end')
            self.entry_ISBN.insert('end', self.selected_tuple[4])
        except IndexError:
            pass


